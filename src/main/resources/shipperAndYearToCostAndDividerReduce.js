function(doc, prev){
	var cost = 0.0;
	
	for(var i=0;i< doc.orderDetails.length;++i){
		var orderDetails = doc.orderDetails[i];
		cost+= (orderDetails.quantity * orderDetails.unitPrice) * (1 - orderDetails.discount);
		prev.divider += orderDetails.quantity;
	}
	
	prev.cost += cost;
}