function(doc, prev){
	var cost = 0.0;
	
	for(var index = 0; index < doc.orderDetails.length; ++index){
		var orderDetails = doc.orderDetails[index];
		cost += (orderDetails.unitPrice * orderDetails.quantity) * (1 - orderDetails.discount);
	}
	prev.cost +=cost;
}