function(){
	var cost = 0.0;
	
	for(var index = 0; index < this.orderDetails.length; ++index){
		var orderDetails = this.orderDetails[index];
		cost += (orderDetails.unitPrice * orderDetails.quantity) * (1 - orderDetails.discount);
	}
	var weekDay = this.orderDate.getUTCDay();
	emit(weekDay, cost);
}