package pl.agh.edu.Petroniusz.sampleoperation;

public class AverageOrderTimeResult {

	private String id;
	private long value;

	public String getId() {
		return id;
	}

	public long getValue() {
		return value;
	}

	public void setValue(long value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "AverageOrderTimeResult [id=" + id + ", value=" + value + "]";
	}

}
