package pl.agh.edu.Petroniusz.sampleoperation;

import pl.agh.edu.northwind.Customer;

public class CustomerToOrderCountResult {

	private Customer id;
	private int value;

	public Customer getId() {
		return id;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "CustomerToOrderCountResult [id=" + id + ", value=" + value + "]";
	}

}
