package pl.agh.edu.Petroniusz.sampleoperation;

import pl.agh.edu.northwind.Product;

public class ProductToOrderedQuantityResult {
	private Product id;
	private int value;

	public Product getId() {
		return id;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "ProductToOrderedQuantityResult [id=" + id + ", value=" + value + "]";
	}
}
