package pl.agh.edu.Petroniusz.sampleoperation;


public class OrderIDToCostResult {
	private String id;
	private double value;

	public String getId() {
		return id;
	}

	public double getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "OrderToCostResult [id=" + id + ", value=" + value + "]";
	}
}
