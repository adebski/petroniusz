package pl.agh.edu.Petroniusz.sampleoperation;

import pl.agh.edu.northwind.Shipper;

public class ShipperAndYearToCostAndDividerResult {
	private Shipper shipper;
	private int year;
	private double cost;
	private int divider;
	public Shipper getShipper() {
		return shipper;
	}
	public void setShipper(Shipper shipper) {
		this.shipper = shipper;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public int getDivider() {
		return divider;
	}
	public void setDivider(int divider) {
		this.divider = divider;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(cost);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + divider;
		result = prime * result + ((shipper == null) ? 0 : shipper.hashCode());
		result = prime * result + year;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ShipperAndYearToCostAndDividerResult other = (ShipperAndYearToCostAndDividerResult) obj;
		if (Double.doubleToLongBits(cost) != Double.doubleToLongBits(other.cost))
			return false;
		if (divider != other.divider)
			return false;
		if (shipper == null) {
			if (other.shipper != null)
				return false;
		} else if (!shipper.equals(other.shipper))
			return false;
		if (year != other.year)
			return false;
		return true;
	}
}
