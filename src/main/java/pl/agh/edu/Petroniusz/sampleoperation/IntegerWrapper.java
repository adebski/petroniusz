package pl.agh.edu.Petroniusz.sampleoperation;

public class IntegerWrapper {

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntegerWrapper other = (IntegerWrapper) obj;
		if (value != other.value)
			return false;
		return true;
	}

	public int value = 0;
}
