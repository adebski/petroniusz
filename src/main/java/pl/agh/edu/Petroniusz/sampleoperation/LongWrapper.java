package pl.agh.edu.Petroniusz.sampleoperation;

public class LongWrapper {
	public long value = 0;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (value ^ (value >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LongWrapper other = (LongWrapper) obj;
		if (value != other.value)
			return false;
		return true;
	}
	
	
}
