package pl.agh.edu.Petroniusz.sampleoperation;

import java.util.List;

public class WeekDayToOrdersResult {
	private int weekDay;
	private List<String> orders;
	
	
	public int getWeekDay() {
		return weekDay;
	}
	public void setWeekDay(int weekDay) {
		this.weekDay = weekDay;
	}
	public List<String> getOrders() {
		return orders;
	}
	public void setOrders(List<String> orders) {
		this.orders = orders;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((orders == null) ? 0 : orders.hashCode());
		result = prime * result + weekDay;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WeekDayToOrdersResult other = (WeekDayToOrdersResult) obj;
		if (orders == null) {
			if (other.orders != null)
				return false;
		} else if (!orders.equals(other.orders))
			return false;
		if (weekDay != other.weekDay)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "YearToOrdersResult [year=" + weekDay + ", orders=" + orders + "]";
	}
	
	
}
