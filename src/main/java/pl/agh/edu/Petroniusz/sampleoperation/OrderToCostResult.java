package pl.agh.edu.Petroniusz.sampleoperation;

import pl.agh.edu.northwind.Order;

public class OrderToCostResult {
	private Order id;
	private double value;

	public Order getId() {
		return id;
	}

	public double getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "OrderToCostResult [id=" + id + ", value=" + value + "]";
	}
}
