package pl.agh.edu.Petroniusz.sampleoperation;

import java.util.List;

import pl.agh.edu.northwind.Shipper;

public class ShipperAndYearToOrdersResult {
	private Shipper shipVia;
	private int year;
	private List<String> orders;
	public Shipper getShipVia() {
		return shipVia;
	}
	public void setShipVia(Shipper shipVia) {
		this.shipVia = shipVia;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public List<String> getOrders() {
		return orders;
	}
	public void setOrders(List<String> orders) {
		this.orders = orders;
	}
	
}
