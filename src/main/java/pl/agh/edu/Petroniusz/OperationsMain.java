package pl.agh.edu.Petroniusz;

import org.apache.log4j.Logger;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;

public class OperationsMain {
	private static final Logger logger = Logger.getLogger(OperationsMain.class);

	public static void main(String[] args) {
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("mongoConfiguration.xml");
		SampleDataOperations sampleDataOperations = context.getBean(SampleDataOperations.class);
		boolean showResults = false;
		sampleDataOperations.ordersByCountriesMapReduce(showResults);
		sampleDataOperations.averageOrderTimeMapReduce(showResults);
		sampleDataOperations.suppliersToProductSoldMapReduce(showResults);
		sampleDataOperations.dayOfTheWeekToOrderCostMapReduce(showResults);
		sampleDataOperations.dayOfTheWeekToOrderCostGroupByAndMapReduce(showResults);
		sampleDataOperations.dayOfTheWeekToOrderCostEmbeddedMapReduce(showResults);
		sampleDataOperations.countryToYearToOrdersCostMapReduce(showResults);
		sampleDataOperations.countryToYearToOrdersCostGroupByAndMapReduce(showResults);
		sampleDataOperations.countryToYearToOrdersCostEmbeddedGroupBy(showResults);
		sampleDataOperations.shipperAndYearToAverageProductPriceEmbedded(showResults);
	}
}
