package pl.agh.edu.Petroniusz;

import java.io.IOException;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.xml.sax.SAXException;

import com.mongodb.WriteConcern;

import pl.agh.edu.northwind.Category;
import pl.agh.edu.northwind.Customer;
import pl.agh.edu.northwind.Employee;
import pl.agh.edu.northwind.Order;
import pl.agh.edu.northwind.OrderDetails;
import pl.agh.edu.northwind.OrderWithOrderDetailsEmbedded;
import pl.agh.edu.northwind.Product;
import pl.agh.edu.northwind.Shipper;
import pl.agh.edu.northwind.Supplier;


public class LoadMain {
	private static final Logger logger = Logger.getLogger(LoadMain.class);
	
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException, ParseException {
		@SuppressWarnings("resource")
		ApplicationContext context = new ClassPathXmlApplicationContext("mongoConfiguration.xml");
/*		TestDatabaseOperations testDatabaseOperations = context.getBean("testDatabaseOperations", TestDatabaseOperations.class);
		testDatabaseOperations.runTests();*/
		//for changing write concerns, we want to test FSYNCED, ACKNOWLEDGED and ERRORS_IGNORED
		MongoTemplate mongoTemplate = context.getBean(MongoTemplate.class);
		mongoTemplate.setWriteConcern(WriteConcern.ERRORS_IGNORED);
		SampleDataSaver sampleDataSaver = context.getBean(SampleDataSaver.class);
		SampleDataLoader dataLoader = new SampleDataLoader();
		
		Map<BigInteger, Supplier> suppliers = dataLoader.loadSuppliers("suppliers.xml");
		Map<BigInteger, Category> categories = dataLoader.loadCategories("categories.xml");
		Map<String, Customer> customers = dataLoader.loadCustomers("customers.xml");
		Map<BigInteger, Shipper> shippers = dataLoader.loadShippers("shippers.xml");
		Map<BigInteger, Product> products = dataLoader.loadProducts("products.xml", suppliers, categories);
		Map<BigInteger, Employee> employees = dataLoader.loadEmployees("employees.xml");

		sampleDataSaver.saveSuppliers(suppliers.values());
		sampleDataSaver.saveCategories(categories.values());
		sampleDataSaver.saveCustomers(customers.values());
		sampleDataSaver.saveShippers(shippers.values());
		sampleDataSaver.saveProducts(products.values());
		sampleDataSaver.saveEmployees(employees.values());
		
		Map<BigInteger, Order> orders = dataLoader.loadOrders("orders_rand_10000.xml", customers, employees, shippers);
		orders.putAll(dataLoader.loadOrders("orders_rand_20000.xml", customers, employees, shippers));
		
		Map<BigInteger, OrderDetails> orderDetails = dataLoader.loadOrderDetails("orderdetails_rand_10000.xml", orders, products);
		orderDetails.putAll(dataLoader.loadOrderDetails("orderdetails_rand_20000.xml", orders, products));
		
		Map<Order, List<OrderDetails>> orderToOrderDetails = buildOrderToOrderDetailsMap(orderDetails);
		
		logger.info("starting to save orders data with unembedded order details");
		long startTime = System.currentTimeMillis();
		sampleDataSaver.saveOrderWithOrderDetailsMeasureTime(orderToOrderDetails);
		long endTime = System.currentTimeMillis();
		long timeElapsed = endTime - startTime;
		System.out.println("milis:"+timeElapsed );
		logger.info("finished saving orders data with unembedded order details");
		
		Map<BigInteger, OrderWithOrderDetailsEmbedded> ordersWithEmbeddedOrderDetails = dataLoader.loadOrdersWithEmbeddedOrderDetails("orders_rand_10000.xml", customers, employees, shippers);
		ordersWithEmbeddedOrderDetails.putAll(dataLoader.loadOrdersWithEmbeddedOrderDetails("orders_rand_20000.xml", customers, employees, shippers));
		
		dataLoader.loadOrderDetailsAndEmbeddThemInOrder("orderdetails_rand_10000.xml", ordersWithEmbeddedOrderDetails, products);
		dataLoader.loadOrderDetailsAndEmbeddThemInOrder("orderdetails_rand_20000.xml", ordersWithEmbeddedOrderDetails, products);
	
		logger.info("starting to save orders data with embedded order details");
		startTime = System.currentTimeMillis();
		sampleDataSaver.saveOrderWithEmbeddedOrderDetailsMeasureTime(ordersWithEmbeddedOrderDetails.values());
		endTime = System.currentTimeMillis();
		timeElapsed = endTime - startTime;
		System.out.println("milis:"+timeElapsed );
		logger.info("finished saving orders data with embedded order details");
	}
	
	private static Map<Order, List<OrderDetails>> buildOrderToOrderDetailsMap(Map<BigInteger, OrderDetails> orderDetails){
		Map<Order, List<OrderDetails>> result = new HashMap<Order, List<OrderDetails>>();
		
		for(OrderDetails orderDetailsInstance: orderDetails.values()){
			Order order = orderDetailsInstance.getOrder();
			
			if (result.containsKey(order) == false){
				result.put(order, new ArrayList<OrderDetails>());
			}
			
			result.get(order).add(orderDetailsInstance);
		}
		
		return result;
	}
}
