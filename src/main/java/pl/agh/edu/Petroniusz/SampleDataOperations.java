package pl.agh.edu.Petroniusz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapreduce.GroupBy;
import org.springframework.data.mongodb.core.mapreduce.GroupByResults;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.stereotype.Service;

import com.mongodb.util.Hash;

import pl.agh.edu.Petroniusz.sampleoperation.AverageOrderTimeResult;
import pl.agh.edu.Petroniusz.sampleoperation.CustomerAndYearToCostResult;
import pl.agh.edu.Petroniusz.sampleoperation.CustomerAndYearToOrderIDListResult;
import pl.agh.edu.Petroniusz.sampleoperation.CustomerToOrderCountResult;
import pl.agh.edu.Petroniusz.sampleoperation.DoubleWrapper;
import pl.agh.edu.Petroniusz.sampleoperation.IntegerWrapper;
import pl.agh.edu.Petroniusz.sampleoperation.LongWrapper;
import pl.agh.edu.Petroniusz.sampleoperation.OrderIDToCostResult;
import pl.agh.edu.Petroniusz.sampleoperation.OrderToAverageProductPriceResult;
import pl.agh.edu.Petroniusz.sampleoperation.OrderToCostResult;
import pl.agh.edu.Petroniusz.sampleoperation.ProductToOrderedQuantityResult;
import pl.agh.edu.Petroniusz.sampleoperation.ShipperAndYearToCostAndDividerResult;
import pl.agh.edu.Petroniusz.sampleoperation.ShipperAndYearToOrdersResult;
import pl.agh.edu.Petroniusz.sampleoperation.WeekDayToOrderCostResult;
import pl.agh.edu.Petroniusz.sampleoperation.WeekDayToOrdersResult;
import pl.agh.edu.northwind.Supplier;
import pl.agh.edu.northwind.repository.SupplierRepository;

@Service
public class SampleDataOperations {

	private static final Logger logger = Logger.getLogger(SampleDataOperations.class);

	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private SupplierRepository supplierRepository;

	public void ordersByCountriesMapReduce(boolean showResults) {

		logger.info("ordersByCountriesMapReduce starting");

		long startTime = System.currentTimeMillis();
		MapReduceResults<CustomerToOrderCountResult> customerToOrderCountResult = mongoTemplate.mapReduce("Orders",
				"classpath:customerToOrderCountMap.js", "classpath:customerToOrderCountReduce.js",
				CustomerToOrderCountResult.class);
		long afterFetching = System.currentTimeMillis() - startTime;
		// Contains country -> count mapping
		Map<String, IntegerWrapper> resultMap = new HashMap<String, IntegerWrapper>();
		for (CustomerToOrderCountResult result : customerToOrderCountResult) {
			String customerCountry = result.getId().getCountry();

			if (resultMap.containsKey(customerCountry) == false) {
				IntegerWrapper integerWrapper = new IntegerWrapper();
				integerWrapper.value = result.getValue();
				resultMap.put(customerCountry, integerWrapper);
			} else {
				resultMap.get(customerCountry).value += result.getValue();
			}
		}
		long endTime = System.currentTimeMillis();
		long elapsedTime = (endTime - startTime);
		System.out.println("milis:" + elapsedTime + " fetching took:" + afterFetching);

		if (showResults) {
			for (String country : resultMap.keySet()) {
				System.out.println(country + ":" + resultMap.get(country).value);
			}
		}

		logger.info("ordersByCountriesMapReduce ended");
	}

	public void averageOrderTimeMapReduce(boolean showResults) {

		logger.info("averageOrderTimeMapReduce starting");

		long startTime = System.currentTimeMillis();
		MapReduceResults<AverageOrderTimeResult> results = mongoTemplate
				.mapReduce("Orders", "classpath:averageOrderTimeMap.js", "classpath:averageOrderTimeReduce.js",
						AverageOrderTimeResult.class);
		long endTime = System.currentTimeMillis();
		long elapsedTime = (endTime - startTime);
		System.out.println("milis:" + elapsedTime + " fetching took milis:" + elapsedTime);

		if (showResults) {
			for (AverageOrderTimeResult resultObject : results) {
				Period period = new Period(resultObject.getValue());
				System.out.println(resultObject.getId() + ":" + PeriodFormat.getDefault().print(period));
			}
		}

		logger.info("averageOrderTimeMapReduce ended");
	}

	public void suppliersToProductSoldMapReduce(boolean showResults) {
		logger.info("suppliersToProductSoldMapReduce starting");

		long startTime = System.currentTimeMillis();
		MapReduceResults<ProductToOrderedQuantityResult> results = mongoTemplate.mapReduce("OrderDetails",
				"classpath:productToOrderedQuantityMap.js", "classpath:productToOrderedQuantityReduce.js",
				ProductToOrderedQuantityResult.class);
		List<Supplier> suppliers = supplierRepository.findAll();
		long afterFetching = System.currentTimeMillis() - startTime;
		Map<Supplier, IntegerWrapper> resultMap = new HashMap<Supplier, IntegerWrapper>();

		for (Supplier supplier : suppliers) {
			resultMap.put(supplier, new IntegerWrapper());
		}

		for (ProductToOrderedQuantityResult productToOrderedQuantityResult : results) {
			resultMap.get(productToOrderedQuantityResult.getId().getSupplier()).value += productToOrderedQuantityResult
					.getValue();
		}
		long endTime = System.currentTimeMillis();
		long elapsedTime = (endTime - startTime);
		System.out.println("milis:" + elapsedTime + " fetching took milis:" + afterFetching);

		if (showResults) {
			for (Supplier supplier : resultMap.keySet()) {
				System.out.println(supplier + ":" + resultMap.get(supplier).value);
			}
		}

		logger.info("suppliersToProductSoldMapReduce ended");
	}

	public void dayOfTheWeekToOrderCostMapReduce(boolean showResults) {
		logger.info("dayOfTheWeekToOrderCostMapReduce starting");

		long startTime = System.currentTimeMillis();
		MapReduceResults<OrderToCostResult> results = mongoTemplate.mapReduce("OrderDetails",
				"classpath:orderToCostMap.js", "classpath:orderToCostReduce.js", OrderToCostResult.class);
		long afterFetching = System.currentTimeMillis() - startTime;
		// joda-time days are one based;
		double result[] = new double[8];

		for (int i = 0; i < result.length; ++i) {
			result[i] = 0;
		}

		for (OrderToCostResult orderToCostResult : results) {
			DateTime dateTime = new DateTime(orderToCostResult.getId().getOrderDate(), DateTimeZone.UTC);
			int dayOfWeek = dateTime.getDayOfWeek();
			// System.out.println(dayOfWeek + ":" +
			// orderToCostResult.getId().getOrderID());
			result[dayOfWeek] += orderToCostResult.getValue();
		}
		long endTime = System.currentTimeMillis();
		long elapsedTime = (endTime - startTime);
		System.out.println("milis:" + elapsedTime + " fetching took milis:" + afterFetching);

		if (showResults) {
			System.out.println("monday:" + result[DateTimeConstants.MONDAY]);
			System.out.println("tuesday:" + result[DateTimeConstants.TUESDAY]);
			System.out.println("wednesday:" + result[DateTimeConstants.WEDNESDAY]);
			System.out.println("thursday:" + result[DateTimeConstants.THURSDAY]);
			System.out.println("friday:" + result[DateTimeConstants.FRIDAY]);
			System.out.println("saturday" + result[DateTimeConstants.SATURDAY]);
			System.out.println("sunday:" + result[DateTimeConstants.SUNDAY]);
		}

		logger.info("dayOfTheWeekToOrderCostMapReduce ended");
	}

	public void dayOfTheWeekToOrderCostGroupByAndMapReduce(boolean showResults) {
		logger.info("dayOfTheWeekToOrderCostGroupByAndMapReduce starting");

		long startTime = System.currentTimeMillis();

		MapReduceResults<OrderIDToCostResult> orderIDToCostResults = mongoTemplate.mapReduce("OrderDetails",
				"classpath:orderIDToCostMap.js", "classpath:orderToCostReduce.js", OrderIDToCostResult.class);
		GroupByResults<WeekDayToOrdersResult> weekDayToOrdersResults = mongoTemplate.group(
				"Orders",
				GroupBy.keyFunction("function(doc){return {weekDay: doc.orderDate.getUTCDay()};}")
						.initialDocument("{ orders: [] }")
						.reduceFunction("function(doc, prev) { prev.orders.push(doc._id) }"),
				WeekDayToOrdersResult.class);
		long afterFetching = System.currentTimeMillis() - startTime;

		HashMap<String, DoubleWrapper> orderIDToCostMap = new HashMap<String, DoubleWrapper>();

		for (OrderIDToCostResult orderIDToCostResult : orderIDToCostResults) {
			DoubleWrapper wrapper = new DoubleWrapper();
			wrapper.value = orderIDToCostResult.getValue();
			orderIDToCostMap.put(orderIDToCostResult.getId(), wrapper);
		}

		// mongoDB days 0 - sunday, 6 - saturday
		double result[] = new double[7];

		for (int i = 0; i < 7; ++i) {
			result[i] = 0;
		}

		for (WeekDayToOrdersResult weekDayToOrdersResult : weekDayToOrdersResults) {
			int weekDay = weekDayToOrdersResult.getWeekDay();
			for (String orderID : weekDayToOrdersResult.getOrders()) {
				result[weekDay] += orderIDToCostMap.get(orderID).value;
			}

		}
		long endTime = System.currentTimeMillis();
		long elapsedTime = (endTime - startTime);
		System.out.println("milis:" + elapsedTime + " fetching took milis:" + afterFetching);

		if (showResults) {
			System.out.println("monday:" + result[1]);
			System.out.println("tuesday:" + result[2]);
			System.out.println("wednesday:" + result[3]);
			System.out.println("thursday:" + result[4]);
			System.out.println("friday:" + result[5]);
			System.out.println("saturday" + result[6]);
			System.out.println("sunday:" + result[0]);
		}

		logger.info("dayOfTheWeekToOrderCostGroupByAndMapReduce ended");
	}

	public void dayOfTheWeekToOrderCostEmbeddedMapReduce(boolean showResults) {
		logger.info("dayOfTheWeekToOrderCostEmbeddedMapReduce starting");

		long startTime = System.currentTimeMillis();

		MapReduceResults<WeekDayToOrderCostResult> weekDayToOrderCostResults = mongoTemplate.mapReduce(
				"OrderWithOrderDetailsEmbedded", "classpath:weekDayToOrderCostMap.js",
				"classpath:weekDayToOrderCostReduce.js", WeekDayToOrderCostResult.class);
		long afterFetching = System.currentTimeMillis() - startTime;

		// mongoDB days 0 - sunday, 6 - saturday
		double result[] = new double[7];

		for (WeekDayToOrderCostResult weekDayToOrdersResult : weekDayToOrderCostResults) {
			int weekDay = weekDayToOrdersResult.getId();
			double value = weekDayToOrdersResult.getValue();
			result[weekDay] = value;
		}
		long endTime = System.currentTimeMillis();
		long elapsedTime = (endTime - startTime);
		System.out.println("milis:" + elapsedTime + " fetching took milis:" + afterFetching);

		if (showResults) {
			System.out.println("monday:" + result[1]);
			System.out.println("tuesday:" + result[2]);
			System.out.println("wednesday:" + result[3]);
			System.out.println("thursday:" + result[4]);
			System.out.println("friday:" + result[5]);
			System.out.println("saturday" + result[6]);
			System.out.println("sunday:" + result[0]);
		}

		logger.info("dayOfTheWeekToOrderCostEmbeddedMapReduce ended");
	}

	public void countryToYearToOrdersCostMapReduce(boolean showResults) {
		logger.info("countryToYearToOrdersCostMapReduce starting");

		long startTime = System.currentTimeMillis();

		MapReduceResults<OrderToCostResult> results = mongoTemplate.mapReduce("OrderDetails",
				"classpath:orderToCostMap.js", "classpath:orderToCostReduce.js", OrderToCostResult.class);
		long afterFetching = System.currentTimeMillis() - startTime;
		Map<String, Map<IntegerWrapper, DoubleWrapper>> resultMap = new HashMap<String, Map<IntegerWrapper, DoubleWrapper>>();

		IntegerWrapper keyWrapper = new IntegerWrapper();
		for (OrderToCostResult orderToCostResult : results) {
			DateTime dateTime = new DateTime(orderToCostResult.getId().getOrderDate());
			int year = dateTime.getYear();
			keyWrapper.value = year;
			String countryName = orderToCostResult.getId().getCustomer().getCountry();

			if (resultMap.containsKey(countryName) == false) {
				resultMap.put(countryName, new HashMap<IntegerWrapper, DoubleWrapper>());
			}
			Map<IntegerWrapper, DoubleWrapper> yearMap = resultMap.get(countryName);

			if (yearMap.containsKey(keyWrapper) == false) {
				IntegerWrapper newKey = new IntegerWrapper();
				newKey.value = keyWrapper.value;
				yearMap.put(newKey, new DoubleWrapper());
			}

			yearMap.get(keyWrapper).value += orderToCostResult.getValue();
		}

		long endTime = System.currentTimeMillis();
		long elapsedTime = (endTime - startTime);
		System.out.println("milis:" + elapsedTime + " fetching took milis:" + afterFetching);

		if (showResults) {
			for (String country : resultMap.keySet()) {
				System.out.println(country);
				Map<IntegerWrapper, DoubleWrapper> yearMap = resultMap.get(country);
				for (IntegerWrapper year : yearMap.keySet()) {
					System.out.println("\t" + year.value + " " + yearMap.get(year).value);
				}
			}
		}

		logger.info("countryToYearToOrdersCostMapReduce ended");
	}

	public void countryToYearToOrdersCostGroupByAndMapReduce(boolean showResults) {
		logger.info("countryToYearToOrdersCostGroupByAndMapReduce starting");

		long startTime = System.currentTimeMillis();

		MapReduceResults<OrderIDToCostResult> orderIDToCostResults = mongoTemplate.mapReduce("OrderDetails",
				"classpath:orderIDToCostMap.js", "classpath:orderToCostReduce.js", OrderIDToCostResult.class);
		GroupByResults<CustomerAndYearToOrderIDListResult> customerAndYearToOrderIDListResults = mongoTemplate.group(
				"Orders",
				GroupBy.keyFunction(
						"function(doc){return {customer: doc.customer, year:doc.orderDate.getUTCFullYear()};}")
						.initialDocument("{ orders: [] }")
						.reduceFunction("function(doc, prev) { prev.orders.push(doc._id) }"),
				CustomerAndYearToOrderIDListResult.class);
		long afterFetching = System.currentTimeMillis() - startTime;
		Map<String, Map<IntegerWrapper, DoubleWrapper>> resultMap = new HashMap<String, Map<IntegerWrapper, DoubleWrapper>>();

		HashMap<String, DoubleWrapper> orderIDToCostMap = new HashMap<String, DoubleWrapper>();

		for (OrderIDToCostResult orderIDToCostResult : orderIDToCostResults) {
			DoubleWrapper wrapper = new DoubleWrapper();
			wrapper.value = orderIDToCostResult.getValue();
			orderIDToCostMap.put(orderIDToCostResult.getId(), wrapper);
		}

		for (CustomerAndYearToOrderIDListResult customerAndYearToOrderIDListResult : customerAndYearToOrderIDListResults) {
			String countryName = customerAndYearToOrderIDListResult.getCustomer().getCountry();

			if (resultMap.containsKey(countryName) == false) {
				resultMap.put(countryName, new HashMap<IntegerWrapper, DoubleWrapper>());
			}
			Map<IntegerWrapper, DoubleWrapper> yearMap = resultMap.get(countryName);
			IntegerWrapper key = new IntegerWrapper();
			key.value = customerAndYearToOrderIDListResult.getYear();

			if (yearMap.containsKey(key) == false) {
				yearMap.put(key, new DoubleWrapper());
			}
			DoubleWrapper cost = yearMap.get(key);

			for (String orderID : customerAndYearToOrderIDListResult.getOrders()) {
				cost.value += orderIDToCostMap.get(orderID).value;
			}
		}

		long endTime = System.currentTimeMillis();
		long elapsedTime = (endTime - startTime);
		System.out.println("milis:" + elapsedTime + " fetching took milis:" + afterFetching);

		if(showResults){
			for (String country : resultMap.keySet()) {
				System.out.println(country);
				Map<IntegerWrapper, DoubleWrapper> yearMap = resultMap.get(country);
				for (IntegerWrapper year : yearMap.keySet()) {
					System.out.println("\t" + year.value + " " + yearMap.get(year).value);
				}
			}
		}


		logger.info("countryToYearToOrdersCostGroupByAndMapReduce ended");
	}

	public void countryToYearToOrdersCostEmbeddedGroupBy(boolean showResults) {
		logger.info("countryToYearToOrdersCostEmbeddedGroupBy starting");

		long startTime = System.currentTimeMillis();

		GroupByResults<CustomerAndYearToCostResult> customerAndYearToOrderIDListResults = mongoTemplate.group(
				"OrderWithOrderDetailsEmbedded",
				GroupBy.keyFunction(
						"function(doc){return {customer: doc.customer, year:doc.orderDate.getUTCFullYear()};}")
						.initialDocument("{ cost: 0.0 }").reduceFunction("classpath:customerAndYearToCostReduce.js"),
				CustomerAndYearToCostResult.class);
		long afterFetching = System.currentTimeMillis() - startTime;
		Map<String, Map<IntegerWrapper, IntegerWrapper>> resultMap = new HashMap<String, Map<IntegerWrapper, IntegerWrapper>>();

		for (CustomerAndYearToCostResult customerAndYearToCostResult : customerAndYearToOrderIDListResults) {
			String countryName = customerAndYearToCostResult.getCustomer().getCountry();

			if (resultMap.containsKey(countryName) == false) {
				resultMap.put(countryName, new HashMap<IntegerWrapper, IntegerWrapper>());
			}
			Map<IntegerWrapper, IntegerWrapper> yearMap = resultMap.get(countryName);
			IntegerWrapper key = new IntegerWrapper();
			key.value = customerAndYearToCostResult.getYear();

			if (yearMap.containsKey(key) == false) {
				yearMap.put(key, new IntegerWrapper());
			}
			IntegerWrapper cost = yearMap.get(key);
			cost.value += customerAndYearToCostResult.getCost();
		}

		long endTime = System.currentTimeMillis();
		long elapsedTime = (endTime - startTime);
		System.out.println("milis:" + elapsedTime + " fetching took milis:" + afterFetching);

		if(showResults){
			for (String country : resultMap.keySet()) {
				System.out.println(country);
				Map<IntegerWrapper, IntegerWrapper> yearMap = resultMap.get(country);
				for (IntegerWrapper year : yearMap.keySet()) {
					System.out.println("\t" + year.value + " " + yearMap.get(year).value);
				}
			}
		}


		logger.info("countryToYearToOrdersCostEmbeddedGroupBy ended");
	}

	public void shipperAndYearToAverageProductPriceEmbedded(boolean showResults) {
		logger.info("shipperAndYearToAverageProductPriceEmbedded starting");

		long startTime = System.currentTimeMillis();

		GroupByResults<ShipperAndYearToCostAndDividerResult> shipperAndYearToCostAndDividerResults = mongoTemplate
				.group("OrderWithOrderDetailsEmbedded",
						GroupBy.keyFunction(
								"function(doc){return {shipper:doc.shipVia, year:doc.orderDate.getUTCFullYear()};}")
								.initialDocument("{ cost: 0, divider: 0 }")
								.reduceFunction("classpath:shipperAndYearToCostAndDividerReduce.js"),
						ShipperAndYearToCostAndDividerResult.class);
		long afterFetching = System.currentTimeMillis() - startTime;

		HashMap<ShipperAndYearToCostAndDividerResult, DoubleWrapper> resultMap = new HashMap<ShipperAndYearToCostAndDividerResult, DoubleWrapper>();

		for (ShipperAndYearToCostAndDividerResult shipperAndYearToCostAndDividerResult : shipperAndYearToCostAndDividerResults) {
			long divider = shipperAndYearToCostAndDividerResult.getDivider();
			double cost = shipperAndYearToCostAndDividerResult.getCost();

			DoubleWrapper doubleWrapper = new DoubleWrapper();
			doubleWrapper.value = cost / divider;
			resultMap.put(shipperAndYearToCostAndDividerResult, doubleWrapper);
		}

		long endTime = System.currentTimeMillis();
		long elapsedTime = (endTime - startTime);

		System.out.println("milis:" + elapsedTime + " fetching took milis:" + afterFetching);

		if(showResults){
			for (ShipperAndYearToCostAndDividerResult shipperAndYearToCostAndDividerResult : resultMap.keySet()) {
				System.out.println(shipperAndYearToCostAndDividerResult.getShipper().getShipperID() + ":"
						+ shipperAndYearToCostAndDividerResult.getYear() + ":"
						+ resultMap.get(shipperAndYearToCostAndDividerResult).value);
			}
		}
		

		
		logger.info("shipperAndYearToAverageProductPriceEmbedded ended");
	}
}
