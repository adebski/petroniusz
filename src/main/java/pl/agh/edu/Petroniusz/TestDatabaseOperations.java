package pl.agh.edu.Petroniusz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.agh.edu.northwind.Category;
import pl.agh.edu.northwind.OrderDetails;
import pl.agh.edu.northwind.Product;
import pl.agh.edu.northwind.Supplier;
import pl.agh.edu.northwind.repository.CategoryRepository;
import pl.agh.edu.northwind.repository.OrderDetailsRepository;
import pl.agh.edu.northwind.repository.ProductRepository;
import pl.agh.edu.northwind.repository.SupplierRepository;

@Service
public class TestDatabaseOperations {

	@Autowired
	private SupplierRepository supplierRepository;
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private OrderDetailsRepository orderDetailsRepository;

	private Random random = new Random();
	
	private BufferedReader bufferedReader = new BufferedReader(
			new InputStreamReader(System.in));

	private void createSuppliersQueryThemAndUpdateOne() throws IOException{
		System.out.println("===running createSuppliersAndQueryThem===");

		Supplier firstSupplier = new Supplier(random.nextLong(), "firstSupplier");
		Supplier secondSupplier = new Supplier(random.nextLong(), "secondSupplier");

		secondSupplier.setPhone("12345");

		supplierRepository.save(firstSupplier);
		supplierRepository.save(secondSupplier);

		List<Supplier> foundSuppliers = supplierRepository
				.findByCompanyName("firstSupplier");

		System.out.println("Found " + foundSuppliers.size()
				+ " suppliers with companyname =  firstSupplier");
		for (Supplier supplier : foundSuppliers) {
			System.out.println("\t" + supplier);
		}

		foundSuppliers = supplierRepository.findByPhone("12345");

		waitForText("Type next to continue:", "next");
		
		System.out.println("Found " + foundSuppliers.size()
				+ " suppliers with phone 12345");
		for (Supplier supplier : foundSuppliers) {
			System.out.println("\t" + supplier);
			supplier.setCompanyName("firstSupplier");
		}
		supplierRepository.save(foundSuppliers);

		foundSuppliers = supplierRepository.findByCompanyName("firstSupplier");
		System.out.println("Found " + foundSuppliers.size()
				+ " suppliers with companyname =  firstSupplier");
	}

	private void createProductAndSaveNormally() {
		System.out.println("===running createProductAndSaveNormally===");

		Supplier supplier = new Supplier(random.nextLong(), "supplier");
		Category category = new Category(random.nextLong(), "category");
		Product product = new Product(random.nextLong(), "product", false);
		ArrayList<OrderDetails> orderDetails = new ArrayList<OrderDetails>();
		OrderDetails firstOrderDetails = new OrderDetails(random.nextLong(), null, null);
		OrderDetails secondOrderDetails = new OrderDetails(random.nextLong(), null, null);

		orderDetails.add(firstOrderDetails);
		orderDetails.add(secondOrderDetails);

		product.setCategory(category);
		product.setSupplier(supplier);

		System.out.println("==Before save==");
		System.out.println("Product id:" + product.getProductID());
		System.out.println("Category id:" + category.getCategoryID());
		System.out.println("Supplier id:" + supplier.getSupplierID());
		System.out.println("Category :" + product.getCategory());
		System.out.println("Supplier :" + product.getSupplier());

		productRepository.save(product);

		System.out.println("==After save==");
		System.out.println("Product id:" + product.getProductID());
		System.out.println("Category id:" + category.getCategoryID());
		System.out.println("Supplier id:" + supplier.getSupplierID());
		System.out.println("Category :" + product.getCategory());
		System.out.println("Supplier :" + product.getSupplier());

		product = productRepository.findOne(product.getProductID());

		System.out.println("==After query==");
		System.out.println("Product id:" + product.getProductID());
		System.out.println("Category id:" + category.getCategoryID());
		System.out.println("Supplier id:" + supplier.getSupplierID());
		System.out.println("Category :" + product.getCategory());
		System.out.println("Supplier :" + product.getSupplier());
	}

	private void createProductAndSaveCascading() {
		System.out.println("===running createProductAndSaveCascading===");

		Supplier supplier = new Supplier(random.nextLong(), "supplier");
		Category category = new Category(random.nextLong(), "category");
		Product product = new Product(random.nextLong(), "product", false);
		ArrayList<OrderDetails> orderDetails = new ArrayList<OrderDetails>();
		OrderDetails firstOrderDetails = new OrderDetails(random.nextLong(), null, null);
		OrderDetails secondOrderDetails = new OrderDetails(random.nextLong(), null, null);

		orderDetails.add(firstOrderDetails);
		orderDetails.add(secondOrderDetails);

		product.setCategory(category);
		product.setSupplier(supplier);

		System.out.println("==Before save==");
		System.out.println("Product id:" + product.getProductID());
		System.out.println("Category id:" + category.getCategoryID());
		System.out.println("Supplier id:" + supplier.getSupplierID());
		System.out.println("Category:" + product.getCategory());
		System.out.println("Supplier:" + product.getSupplier());

		productRepository.saveCascading(product);

		System.out.println("==After save==");
		System.out.println("Product id:" + product.getProductID());
		System.out.println("Category id:" + category.getCategoryID());
		System.out.println("Supplier id:" + supplier.getSupplierID());
		System.out.println("Category:" + product.getCategory());
		System.out.println("Supplier:" + product.getSupplier());

		product = productRepository.findOne(product.getProductID());

		System.out.println("==After query==");
		System.out.println("Product id:" + product.getProductID());
		System.out.println("Category id:" + category.getCategoryID());
		System.out.println("Supplier id:" + supplier.getSupplierID());
		System.out.println("Category:" + product.getCategory());
		System.out.println("Supplier:" + product.getSupplier());
	}

	private void createProductAndDeleteNormally() {
		System.out.println("===running createProductAndDeleteNormally===");

		Supplier supplier = new Supplier(random.nextLong(), "supplier");
		Category category = new Category(random.nextLong(), "category");
		Product product = new Product(random.nextLong(), "product", false);

		product.setCategory(category);
		product.setSupplier(supplier);

		productRepository.saveCascading(product);

		System.out.println("==Before delete==");
		System.out.println("Product:"
				+ productRepository.findOne(product.getProductID()));
		System.out.println("Category:"
				+ categoryRepository.findOne(category.getCategoryID()));
		System.out.println("Supplier:"
				+ supplierRepository.findOne(supplier.getSupplierID()));

		productRepository.delete(product);

		System.out.println("==After delete==");
		System.out.println("Product:"
				+ productRepository.findOne(product.getProductID()));
		System.out.println("Category:"
				+ categoryRepository.findOne(category.getCategoryID()));
		System.out.println("Supplier:"
				+ supplierRepository.findOne(supplier.getSupplierID()));
	}

	private void clearDatabase() {
		productRepository.deleteAll();
		supplierRepository.deleteAll();
		categoryRepository.deleteAll();
	}

	private boolean isTextTyped(String text) throws IOException {
		return bufferedReader.readLine().equals(text);
	}

	private void waitForText(String prompt, String text) throws IOException {
		do {
			System.out.print(prompt);
		} while (isTextTyped(text) == false);
	}

	private void waitAndClearDatabase() throws IOException {
		waitForText("Type next to continue:", "next");
		clearDatabase();
	}

	public void runTests() throws IOException {
		waitForText("Type next to start tests:", "next");
		createSuppliersQueryThemAndUpdateOne();
		waitAndClearDatabase();
		createProductAndSaveNormally();
		waitAndClearDatabase();
		createProductAndSaveCascading();
		waitAndClearDatabase();
		createProductAndDeleteNormally();
		waitAndClearDatabase();
		System.out.println("Runned all tests, exiting.");
	}
}
