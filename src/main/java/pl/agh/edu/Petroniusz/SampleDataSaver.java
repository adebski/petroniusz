package pl.agh.edu.Petroniusz;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.agh.edu.northwind.Category;
import pl.agh.edu.northwind.Customer;
import pl.agh.edu.northwind.Employee;
import pl.agh.edu.northwind.Order;
import pl.agh.edu.northwind.OrderDetails;
import pl.agh.edu.northwind.OrderWithOrderDetailsEmbedded;
import pl.agh.edu.northwind.Product;
import pl.agh.edu.northwind.Shipper;
import pl.agh.edu.northwind.Supplier;
import pl.agh.edu.northwind.repository.CategoryRepository;
import pl.agh.edu.northwind.repository.CustomerRepository;
import pl.agh.edu.northwind.repository.EmployeeRepository;
import pl.agh.edu.northwind.repository.OrderDetailsRepository;
import pl.agh.edu.northwind.repository.OrderRepository;
import pl.agh.edu.northwind.repository.OrderWithOrderDetailsEmbeddedRepository;
import pl.agh.edu.northwind.repository.ProductRepository;
import pl.agh.edu.northwind.repository.ShipperRepository;
import pl.agh.edu.northwind.repository.SupplierRepository;

@Service
public class SampleDataSaver {
	
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	private OrderDetailsRepository orderDetailsRepository;
	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private ProductRepository productRepository;
	@Autowired
	private ShipperRepository shipperRepository;
	@Autowired
	private SupplierRepository supplierRepository;
	@Autowired
	private OrderWithOrderDetailsEmbeddedRepository orderWithOrderDetailsEmbeddedRepository;
	
	public void saveSuppliers(Collection<Supplier> suppliers){
		supplierRepository.save(suppliers);
	}
	
	public void saveCategories(Collection<Category> categories){
		categoryRepository.save(categories);
	}
	
	public void saveCustomers(Collection<Customer> customers){
		customerRepository.save(customers);
	}
	
	public void saveShippers(Collection<Shipper> shippers){
		shipperRepository.save(shippers);
	}
	
	public void saveProducts(Collection<Product> products){
		productRepository.save(products);
	}
	
	public void saveEmployees(Collection<Employee> employees){
		employeeRepository.save(employees);
	}
	
	public void saveOrderWithOrderDetailsMeasureTime(Map<Order, List<OrderDetails>> ordersMap){
		Iterator<Order> orderIterator = ordersMap.keySet().iterator();
		Order order;
		List<OrderDetails> orderDetails;
		int round = 0;
		long overallElapsedTime = 0;
		
		System.out.println("round:time_elapsed[milis]:average_in_round[milis]");
		while(orderIterator.hasNext()){
			
			if (round == 30){
				break;
			}
			
			long startTime = System.currentTimeMillis();
			for(int i=0;i<1000;++i){
				order = orderIterator.next();
				orderDetails = ordersMap.get(order);
				orderRepository.save(order);
				orderDetailsRepository.save(orderDetails);
			}
			long endTime = System.currentTimeMillis();
			long timeElapsed = (endTime - startTime) ;
			overallElapsedTime +=timeElapsed;
			System.out.println(round+1 + ":" + timeElapsed + ":" + timeElapsed / 1000.0);
			++round;
		}
		
		order = orderIterator.next();
		orderDetails = ordersMap.get(order);
		orderRepository.save(order);
		orderDetailsRepository.save(orderDetails);

		System.out.println("Overall:"+overallElapsedTime + " avg:" + overallElapsedTime / 30000.0);
	}
	
	public void saveOrderWithEmbeddedOrderDetailsMeasureTime(Collection<OrderWithOrderDetailsEmbedded> orders){
		Iterator<OrderWithOrderDetailsEmbedded> orderIterator = orders.iterator();
		OrderWithOrderDetailsEmbedded order;
		int round = 0;
		long overallElapsedTime = 0;
		
		System.out.println("round:time_elapsed[milis]:average_in_round[milis]");
		while(orderIterator.hasNext()){
			
			if (round == 30){
				break;
			}
			
			long startTime = System.currentTimeMillis();
			for(int i=0;i<1000;++i){
				order = orderIterator.next();
				orderWithOrderDetailsEmbeddedRepository.save(order);
			}
			long endTime = System.currentTimeMillis();
			long timeElapsed = (endTime - startTime) ;
			overallElapsedTime +=timeElapsed;
			System.out.println(round+1 + ":" + timeElapsed + ":" + timeElapsed / 1000.0);
			++round;
		}
		
		order = orderIterator.next();
		orderWithOrderDetailsEmbeddedRepository.save(order);

		System.out.println("Overall:"+overallElapsedTime + " avg:" + overallElapsedTime / 30000.0);
	}
}
