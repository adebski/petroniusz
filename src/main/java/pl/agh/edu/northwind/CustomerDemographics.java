package pl.agh.edu.northwind;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class CustomerDemographics {

	@Id
	private ObjectId id;
	@DBRef
	private Collection <Customer> customers;
	private String customerDescription;
	
	public CustomerDemographics(String customerDescription){
		this.id = new ObjectId();
		this.customers = new ArrayList<Customer>();
		this.customerDescription = customerDescription;
	}
	
	public ObjectId getId() {
		return id;
	}

	public Collection<Customer> getCustomers() {
		return customers;
	}
	
	public void setCustomers(Collection<Customer> customers) {
		this.customers = customers;
	}
	
	public void addCustomers(Customer customers){
		this.customers.add(customers);
	}
	
	public void addCustomers(List<Customer> customers){
		this.customers.addAll(customers);
	}
	
	public String getCustomerDescription(){
		return customerDescription;
	}
}
