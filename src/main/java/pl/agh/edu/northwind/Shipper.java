package pl.agh.edu.northwind;

import java.math.BigInteger;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="Shippers")
public class Shipper {
	
	@Id
	private BigInteger shipperID;
	
	private String companyName;
	
	private String phone;

	public Shipper(){
		this(null);
	}
	
	public Shipper(BigInteger shipperID){
		this.shipperID = shipperID;
	}

	public BigInteger getShipperID() {
		return shipperID;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((companyName == null) ? 0 : companyName.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result
				+ ((shipperID == null) ? 0 : shipperID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Shipper other = (Shipper) obj;
		if (companyName == null) {
			if (other.companyName != null)
				return false;
		} else if (!companyName.equals(other.companyName))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (shipperID == null) {
			if (other.shipperID != null)
				return false;
		} else if (!shipperID.equals(other.shipperID))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Shipper [shipperID=" + shipperID + ", companyName="
				+ companyName + ", phone=" + phone + "]";
	}

	

	
}
