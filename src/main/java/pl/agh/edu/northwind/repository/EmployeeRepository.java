package pl.agh.edu.northwind.repository;

import java.math.BigInteger;

import org.springframework.data.mongodb.repository.MongoRepository;

import pl.agh.edu.northwind.Employee;

public interface EmployeeRepository extends MongoRepository<Employee, BigInteger>{

}
