package pl.agh.edu.northwind.repository;

import java.math.BigInteger;

import org.springframework.data.mongodb.repository.MongoRepository;

import pl.agh.edu.northwind.OrderWithOrderDetailsEmbedded;

public interface OrderWithOrderDetailsEmbeddedRepository extends MongoRepository<OrderWithOrderDetailsEmbedded, BigInteger>{

}
