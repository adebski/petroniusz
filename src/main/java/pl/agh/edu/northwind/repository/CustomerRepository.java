package pl.agh.edu.northwind.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import pl.agh.edu.northwind.Customer;

public interface CustomerRepository extends MongoRepository<Customer, String>{

}
