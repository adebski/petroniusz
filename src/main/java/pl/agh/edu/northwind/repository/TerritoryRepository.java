package pl.agh.edu.northwind.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import pl.agh.edu.northwind.Territory;

public interface TerritoryRepository extends MongoRepository<Territory, ObjectId>{

}
