package pl.agh.edu.northwind.repository;

import java.math.BigInteger;

import org.springframework.data.mongodb.repository.MongoRepository;

import pl.agh.edu.northwind.Shipper;

public interface ShipperRepository extends MongoRepository<Shipper, BigInteger>{

}
