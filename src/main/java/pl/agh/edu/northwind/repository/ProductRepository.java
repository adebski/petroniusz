package pl.agh.edu.northwind.repository;

import java.math.BigInteger;

import org.springframework.data.mongodb.repository.MongoRepository;

import pl.agh.edu.northwind.Product;

public interface ProductRepository extends MongoRepository<Product, BigInteger>, ProductRepositoryExtension{

}
