package pl.agh.edu.northwind.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import pl.agh.edu.northwind.Supplier;

public interface SupplierRepository extends MongoRepository<Supplier, BigInteger>{

	List<Supplier> findByCompanyName(String companyName);
	
	List<Supplier> findByPhone(String phoneNumber);
}
