package pl.agh.edu.northwind.repository;

import java.util.List;

import pl.agh.edu.northwind.Product;

public interface ProductRepositoryExtension {
	
	public void saveCascading(Product product);
	public void saveCascading(List<Product> product);
}
