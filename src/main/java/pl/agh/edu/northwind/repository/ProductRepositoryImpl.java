package pl.agh.edu.northwind.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import pl.agh.edu.northwind.Product;


public class ProductRepositoryImpl implements ProductRepositoryExtension{
	
	@Autowired
	private SupplierRepository supplierRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private ProductRepository productRepository;

	
	@Override
	public void saveCascading(List<Product> products) {
		for(Product product: products ){
			saveCascading(product);
		}
	}

	@Override
	public void saveCascading(Product product) {
		supplierRepository.save(product.getSupplier());
		categoryRepository.save(product.getCategory());
		productRepository.save(product);
	}
}
