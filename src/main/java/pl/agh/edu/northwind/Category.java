package pl.agh.edu.northwind;

import java.math.BigInteger;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Categories")
public class Category {

	@Id
	private BigInteger categoryID;

	private String description;

	private String categoryName;

	private byte[] picture;


	public Category(){
		this(null, null);
	}
	
	public Category(long categoryId, String categoryName){
		this(BigInteger.valueOf(categoryId), categoryName);
	}
	
	public Category(BigInteger categoryId, String categoryName) {
		this.categoryID = categoryId;
		this.categoryName = categoryName;
	}


	public BigInteger getCategoryID() {
		return categoryID;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((categoryID == null) ? 0 : categoryID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (categoryID == null) {
			if (other.categoryID != null)
				return false;
		} else if (!categoryID.equals(other.categoryID))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Category [categoryId=" + categoryID + ", categoryName="
				+ categoryName + "]";
	}

}
