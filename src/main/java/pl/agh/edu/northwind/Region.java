package pl.agh.edu.northwind;

import java.util.ArrayList;
import java.util.Collection;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Region {
	
	@Id
	private ObjectId id;
	private String regionDescription;
	@DBRef
	private Collection<Territory> territories;
	
	public Region(String regionDescription){
		this.id = new ObjectId();
		this.regionDescription = regionDescription;
		this.territories = new ArrayList<Territory>();
	}
	
	public Collection<Territory> getTerritories() {
		return territories;
	}
	public void setTerritories(Collection<Territory> territories) {
		this.territories = territories;
	}
	public ObjectId getRegionID() {
		return id;
	}

	public String getRegionDescription() {
		return regionDescription;
	}
	public void setRegionDescription(String regionDescription) {
		this.regionDescription = regionDescription;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Region other = (Region) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Region [id=" + id + ", regionDescription=" + regionDescription
				+ ", territories=" + territories + "]";
	}
}
