package pl.agh.edu.northwind;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="OrderDetails")
@CompoundIndexes({@CompoundIndex(name = "orderDetails_idx", def = "{'product': 1, 'order': 1, '_id':1}", unique=true)})
public class OrderDetails {

	@Id
	private BigInteger orderDetailsID;

	private BigDecimal unitPrice;

	private Integer quantity;

	private Float discount;

	@DBRef
	private Product product;
	@DBRef
	private Order order;

	public OrderDetails() {
		this(null, null, null);
	}

	public OrderDetails(long orderDetailsID, Order order, Product product){
		this(BigInteger.valueOf(orderDetailsID), order, product);
	}
	
	public OrderDetails(BigInteger orderDetailsID, Order order, Product product) {
		this.orderDetailsID = orderDetailsID;
		this.product = product;
		this.order = order;
	}



	public BigInteger getOrderDetailsID() {
		return orderDetailsID;
	}

	public Product getProduct() {
		return product;
	}

	public Order getOrder() {
		return order;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Float getDiscount() {
		return discount;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	public Product getProducts() {
		return product;
	}

	public void setProducts(Product products) {
		this.product = products;
	}

	public Order getOrders() {
		return order;
	}

	public void setOrders(Order orders) {
		this.order = orders;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((order == null) ? 0 : order.hashCode());
		result = prime * result
				+ ((orderDetailsID == null) ? 0 : orderDetailsID.hashCode());
		result = prime * result + ((product == null) ? 0 : product.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderDetails other = (OrderDetails) obj;
		if (order == null) {
			if (other.order != null)
				return false;
		} else if (!order.equals(other.order))
			return false;
		if (orderDetailsID == null) {
			if (other.orderDetailsID != null)
				return false;
		} else if (!orderDetailsID.equals(other.orderDetailsID))
			return false;
		if (product == null) {
			if (other.product != null)
				return false;
		} else if (!product.equals(other.product))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OrderDetails [orderDetailsID=" + orderDetailsID + ", product="
				+ product + ", order=" + order + "]";
	}
}
